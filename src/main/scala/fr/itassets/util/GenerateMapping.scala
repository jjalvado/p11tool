package fr.itassets.util

import java.io.{PrintWriter, FileInputStream, File}
import java.security.cert.{CertificateFactory, X509Certificate}
import java.security.interfaces.RSAPublicKey

import play.api.libs.json.Json
import scopt.OptionParser

case class GenerateMappingConfig(in: String = "", out: String = "")

object GenerateMapping extends App {

  val mapping: collection.mutable.Map[String, String] = collection.mutable.Map()

  val parser = new OptionParser[GenerateMappingConfig]("p11tool -main fr.itassets.util.GenerateMapping") {
    head("GenerateMapping", "1.0")
    opt[String]('i', "in") required() valueName ("<Certificate Folder>") action { (x, c) => c.copy(in = x) } text ("Certificate Folder (required)")
    opt[String]('o', "out") required() valueName ("<Mapping File Path>") action { (x, c) => c.copy(out = x) } text ("Mapping File (required)")
  }

  // Parsing arguments
  parser.parse(args, GenerateMappingConfig()) match {
    case Some(config) => {
      try {
        // Listing Files in the in directory
        val certs = getListOfFiles(config.in)
        certs foreach { c =>
          loadCertificate(c) match {
            case Some(cert) => {
              val dn = cert.getSubjectX500Principal.toString
              val pubKey = cert.getPublicKey.asInstanceOf[RSAPublicKey]
              val modulus = iaik.pkcs.pkcs11.Util.unsignedBigIntergerToByteArray(pubKey.getModulus)
              mapping += (modulus.map("%02x".format(_)).mkString(":").toUpperCase -> dn)
            }
            case None => println(s"File '${c.getPath}' is not a X509 Certificate. Discarding...")
          }
        }
        new PrintWriter(config.out) { f =>
          f.write(Json.toJson(mapping).toString())
          f.close
        }
        println(s"[INFOS] Mapping file '${config.out}' generated")
      } catch {
        case e: Throwable => println(s"[ERROR] Unable to generate mapping file '${config.out}': ${e.getMessage}")
      }

    }
    case None =>
  }

  private def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }

  private def loadCertificate(cert: File): Option[X509Certificate] = {
    val certificateFactory = CertificateFactory.getInstance("X509")
    val certificateStream = new FileInputStream(cert)
    Option(certificateFactory.generateCertificate(certificateStream).asInstanceOf[X509Certificate])
  }

}
