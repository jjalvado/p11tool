package fr.itassets.p11tool

import java.io.FileInputStream
import java.security.KeyStore

import iaik.pkcs.pkcs11._
import iaik.pkcs.pkcs11.objects.RSAPrivateKey
import iaik.pkcs.pkcs11.wrapper.PKCS11Constants
import play.api.libs.json.Json
import scopt.OptionParser

import scala.io.Source
import scala.util.{Failure, Success, Try}

case class P11ToolConfig(pkcs11Lib: String = "", slotId: Long = 0, slotPassword: String = "", action: String = "", debug: Boolean = false, modulusMapping: String = "", dryRun: Boolean = false, p12Path: String  = "", p12Password: String ="", remove: Boolean = false)

object P11Tool extends App {

  val parser = new OptionParser[P11ToolConfig]("p11tool") {
    head("p11tool", "1.1")
    // Global Options
    opt[String]('l', "library") required() valueName("\"PKCS#11 Library Path\"") action { (x, c) => c.copy(pkcs11Lib = x) } text("PKCS#11 Module Library Path (required)")
    opt[Long]('s', "slot") required() valueName("\"PKCS#11 Slot ID\"") action { (x, c) => c.copy(slotId = x) } validate { x => if (x >= 0) success else failure("Slot ID cannot be negative") } text("PKCS#11 Slot ID (required)")
    opt[String]('p', "password") required() valueName("\"PKCS#11 Slot Password\"") action { (x, c) => c.copy(slotPassword = x) } text("PKCS#11 Slot Password (required)")
    opt[String]('a', "action") required() valueName("\"PKCS#11 Action\"") action { (x, c) => c.copy(action = x) } validate { x => if (x.toLowerCase.matches("list|remove-duplicates|import-key")) success else failure (s"Unsupported action '$x'")} text("Action:\n\t\t\"list\": List the RSA private key(s) modulus in token\n\t\t\"remove-duplicates\": remove the duplicated RSA private key(s) from the token\n\t\t\"import-key\": Import a private key from a PKCS#12 file")
    opt[Unit]('g', "debug") optional() action { (_, c) => c.copy(debug = true) } text("Debug Mode (optional)")
    // Action "list" options
    opt[Unit]('r', "remove-unknown") optional() action { (_, c) => c.copy(remove = true) } text("Remove unknown keys (optional and usable only with list action when modulus mapping is enabled)")
    // Action "list" & "remove-duplicates" options
    opt[String]('m', "mapping") optional() valueName("\"Modulus Mapping File Path\" (actions list and remove-duplicates)") action { (x, c) => c.copy(modulusMapping = x) } text("Modulus Mapping File Path (optional and usable only with list and remove-duplicates actions)")
    // Action "remove-duplicates" options
    opt[Unit]('y', "dry-run") optional() action { (_, c) => c.copy(dryRun = true) } text("Dry Run Mode (optional and usable only with remove-duplicates action)")
    opt[String]('i', "in") optional() valueName("\"PKCS#12 Path\"") action { (x, c) => c.copy(p12Path = x) } text("PKCS#12 Path (required for import-key action)")
    opt[String]('x', "p12-password") optional() valueName("\"PKCS#12 Password\"") action { (x, c) => c.copy(p12Password = x) } text("PKCS#12 Password (required for import-key action)")
  }

  // Parsing arguments
  parser.parse(args, P11ToolConfig()) match {
    case Some(config) => {

      // Registering Module
      val module = Try(registerModule(config.pkcs11Lib))
      module match {
        case Success(m) =>
        case Failure (f) => {
          println(s"[ERROR] unable to Load module '${config.pkcs11Lib}': ${f.getMessage}")
          f.printStackTrace()
          System.exit(-1)
        }
      }

      // Opening Session
      val session = Try(openTokenSession(module.get, config.slotId, config.slotPassword, config.debug))
      session match {
        case Success(s) =>
        case Failure(f) => {
          println(s"[ERROR] unable to open Token Session on Slot ${config.slotId}': ${f.getMessage}")
          f.printStackTrace()
          unregisterModule(module)
          System.exit(-1)
        }
      }

      // Dealing with actions
      try {
        config.action.toLowerCase match {
          case "list" => actionList(session.get, config.modulusMapping, config.debug, config.remove)
          case "remove-duplicates" => actionRemoveDuplicates(session.get, config.modulusMapping, config.debug, config.dryRun)
          case "import-key" => actionImportKey(session.get, config.debug, config.p12Path, config.p12Password)
          case _ =>
        }
      } catch {
        case e: Throwable => {
          println(s"[ERROR] Unable to complete action '${config.action}': ${e.getMessage}")
          if (config.debug)
            e.printStackTrace()
        }
      }

      // Closing session
      closeTokenSession(session)

      // Un registering Module
      unregisterModule(module)
    }
    case None =>
  }

  private def registerModule(pkcs11Lib: String): Module = {
    // Instantiating Module
    val pkcs11InitArgs = new DefaultInitializeArgs()
    val pkcs11Module = Module.getInstance(pkcs11Lib)

    // Initializing Module
    pkcs11Module.initialize(pkcs11InitArgs)
    pkcs11Module
  }

  private def unregisterModule(module: Try[Module]) = {
    module.map(_.finalize(null))
  }

  private def openTokenSession(module: Module, slotId: Long, slotPassword: String, debug: Boolean): Session = {
    // Retrieving Slot
    val slot = module.getSlotList(Module.SlotRequirement.TOKEN_PRESENT).filter(_.getSlotID == slotId).head

    // Retrieving Token
    val token = slot.getToken

    // Retrieving Token Infos
    val tokenInfo = token.getTokenInfo
    if (debug)
      println(s"[INFOS] Token Infos:\n${tokenInfo.toString}")

    // Opening session
    val session = token.openSession(Token.SessionType.SERIAL_SESSION, Token.SessionReadWriteBehavior.RW_SESSION, null, null)
    // Log in in if necessary
    if (tokenInfo.isLoginRequired)
      session.login(Session.UserType.USER, slotPassword.toCharArray)
    session
  }

  private def closeTokenSession(session: Try[Session]) = {
    session map { s =>
      s.logout()
      s.closeSession()
    }
  }

  private def loadModulusMapping(modulusMapping: String, debug: Boolean): Option[Map[String, String]] = {
    if (modulusMapping.equalsIgnoreCase(""))
      None
    else {
      try {
        val mappingSource = Source.fromFile(modulusMapping).getLines.mkString
        Some(Json.parse(mappingSource).as[Map[String, String]])
      } catch {
        case e: Throwable => {
          println(s"[ERROR] Unable to parse json from file '${modulusMapping}': ${e.getMessage}")
          if (debug)
            e.printStackTrace()
          None
        }
      }
    }
  }

  private def actionList(session: Session, modulusMapping: String, debug: Boolean, remove: Boolean) = {
    val keysToDelete: collection.mutable.ArrayBuffer[RSAPrivateKey] = collection.mutable.ArrayBuffer.empty[RSAPrivateKey]
    // Loading the Modulus Mapping file
    val mapping = loadModulusMapping(modulusMapping, debug)
    var counter = 0
    // Preparing template to find RSA Private key(s)
    val privateKeyTemplate = new RSAPrivateKey()
    privateKeyTemplate.getPrivate.setBooleanValue(true)
    println("[INFOS] Listing private keys:")
    // Searching for the RSA Private Key(s)
    session.findObjectsInit(privateKeyTemplate)
    def printKeyModulus {
      while(true) {
        val keys = session.findObjects(1)
        if (keys.length <= 0) {
          return
        } else {
          counter +=1
          val key = keys(0).asInstanceOf[RSAPrivateKey]
          if (debug)
            println(s"[INFOS] Key infos:\n${key.toString}")
          val modulus = key.getModulus.getByteArrayValue.map("%02x".format(_)).mkString(":").toUpperCase
          println(s"\t[key $counter]: $modulus --> ${mapping.flatMap(_.get(modulus)).getOrElse("Unknown Key")}")
          mapping match {
            case Some(m) => {
              if (m.get(modulus).isEmpty) {
                println("[INFOS] Key is unknown, adding to the deletion list")
                keysToDelete += key
              }
            }
            case None =>
          }
        }
      }
    }
    printKeyModulus
    session.findObjectsFinal()
    println(s"[INFOS] $counter key(s) detected")
    if (remove) {
      var counter = 0
      println("[INFOS] Deleting unknown...")
      keysToDelete foreach { k =>
        counter += 1
        session.destroyObject(k)
      }
      println(s"[INFOS] $counter key(s) deleted")
    }
  }

  private def actionRemoveDuplicates(session: Session, modulusMapping: String, debug: Boolean, dryRun: Boolean) = {
    val registeredModulus: collection.mutable.Map[String, Int] = collection.mutable.Map()
    val keysToDelete: collection.mutable.ArrayBuffer[RSAPrivateKey] = collection.mutable.ArrayBuffer.empty[RSAPrivateKey]
    // Loading the Modulus Mapping file
    val mapping = loadModulusMapping(modulusMapping, debug)
    // Preparing template to find RSA Private key(s)
    val privateKeyTemplate = new RSAPrivateKey()
    privateKeyTemplate.getPrivate.setBooleanValue(true)
    println("[INFOS] Removing duplicated private key(s):")
    // Searching for the RSA Private Key(s)
    session.findObjectsInit(privateKeyTemplate)
    def detectDuplicatedKeys {
      while(true) {
        val keys = session.findObjects(1)
        if (keys.length <= 0) {
          return
        } else {
          val key = keys(0).asInstanceOf[RSAPrivateKey]
          val modulus = key.getModulus.getByteArrayValue.map("%02x".format(_)).mkString(":").toUpperCase
          val alias = mapping.flatMap(_.get(modulus)).getOrElse(modulus)
          registeredModulus.get(alias) match {
            case Some(occurences) => {
              // Incrementing the Modulus occurence
              registeredModulus += (alias -> (occurences + 1))
              // registering the key for deletion
              keysToDelete += key
              if (debug)
                println(s"[INFOS] Detected a new occurence of key '$alias' (occurence ${occurences + 1})")
            }
            // Detecting th key for the first time
            case None => {
              if (debug)
                println(s"[INFOS] Detected first occurence of key '$alias'")
              registeredModulus += (alias -> 1)
            }
          }
        }
      }
    }
    detectDuplicatedKeys
    session.findObjectsFinal()
    registeredModulus foreach { case(k, v) =>
      if (v > 1) {
        println(s"\t[key $k]: ${v -1} duplicates")
      } else {
        println(s"\t[key $k]: No duplicate")
      }
    }
    if (!dryRun) {
      var counter = 0
      println("[INFOS] Deleting duplicates...")
      keysToDelete foreach { k =>
        counter += 1
        session.destroyObject(k)
      }
      println(s"[INFOS] $counter key(s) deleted")
    }
  }

  private def getPKCS12PrivateKey(p12Path: String, p12Password: String, debug: Boolean): Option[java.security.interfaces.RSAPrivateKey] = {
    try {
      val p12File = new FileInputStream(p12Path)
      val p12 = KeyStore.getInstance("pkcs12")
      p12.load(p12File, p12Password.toCharArray)
      val aliases = p12.aliases()
      var alias: String = ""
      while(aliases.hasMoreElements) {
        alias = aliases.nextElement()
      }
      Option(p12.getKey(alias, p12Password.toCharArray).asInstanceOf[java.security.interfaces.RSAPrivateKey])
    } catch {
      case e: Throwable => {
        println(s"[ERROR] Unable to parse PKCS#12 from file '${p12Path}': ${e.getMessage}")
        if (debug)
          e.printStackTrace()
        None
      }
    }
  }

  private def getPKCS12PublicKey(p12Path: String, p12Password: String, debug: Boolean): Option[java.security.interfaces.RSAPublicKey] = {
    try {
      val p12File = new FileInputStream(p12Path)
      val p12 = KeyStore.getInstance("pkcs12")
      p12.load(p12File, p12Password.toCharArray)
      val aliases = p12.aliases()
      var alias: String = ""
      while(aliases.hasMoreElements) {
        alias = aliases.nextElement()
      }
      Option(p12.getCertificate(alias).getPublicKey.asInstanceOf[java.security.interfaces.RSAPublicKey])
    } catch {
      case e: Throwable => {
        println(s"[ERROR] Unable to parse PKCS#12 from file '${p12Path}': ${e.getMessage}")
        if (debug)
          e.printStackTrace()
        None
      }
    }
  }

  private def importKey(session: Session, privKey: java.security.interfaces.RSAPrivateKey, pubKey: java.security.interfaces.RSAPublicKey, debug: Boolean) = {
    val p11Key = new RSAPrivateKey
    val crtPrivateKey = privKey.asInstanceOf[java.security.interfaces.RSAPrivateCrtKey]

    // Defining RSA Private Key template here
    // CKA_KEY_GEN_MECHANISM
    //p11Key.getKeyGenMechanism.setMechanism(Mechanism.get(PKCS11Constants.CKM_RSA_PKCS_KEY_PAIR_GEN))
    // CKA_SENSITIVE --> TRUE
    p11Key.getSensitive.setBooleanValue(true)
    // CKA_EXTRACTABLE --> FALSE
    p11Key.getExtractable.setBooleanValue(false)
    // CKA_MODIFIABLE --> FALSE
    p11Key.getModifiable.setBooleanValue(true)
    // CKA_TOKEN --> TRUE
    p11Key.getToken.setBooleanValue(true)
    // CKA_PRIVATE --> TRUE
    p11Key.getPrivate.setBooleanValue(true)
    // CKA_LABEL --> ?
    //p11Key.getLabel.setCharArrayValue("label".toCharArray)
    // CKA_ID --> PKCS#11
    p11Key.getId.setByteArrayValue(Array[Byte](0x50, 0x4b, 0x43, 0x53, 0x23, 0x31, 0x31, 0x00))
    // CKA_SIGN --> TRUE
    p11Key.getSign.setBooleanValue(true)
    // CKA_SIGN_RECOVER --> FALSE
    p11Key.getSignRecover().setBooleanValue(false)
    // CKA_DECRYPT --> FALSE
    p11Key.getDecrypt.setBooleanValue(true)
    // CKA_DERIVE --> FALSE
    p11Key.getDerive.setBooleanValue(false)
    // CKA_UNWRAP --> FALSE
    p11Key.getUnwrap.setBooleanValue(false)
    // CKA_ALWAYS_SENSITIVE --> FALSE
    //p11Key.getAlwaysSensitive.setBooleanValue(false)
    // CKA_NEVER_EXTRACTABLE --> FALSE
    //p11Key.getNeverExtractable.setBooleanValue(false)
    // CKA_WRAP_WWITH_TRUSTED --> FALSE
    p11Key.getWrapWithTrusted.setBooleanValue(false)
    // CKA_ALWAYS_AUTHENTICATE --> FALSE
    p11Key.getAlwaysAuthenticate.setBooleanValue(false)


    // Setting attributes from PKCS12 extracted private key
    // CKA_MODULUS
    p11Key.getModulus.setByteArrayValue(iaik.pkcs.pkcs11.Util.unsignedBigIntergerToByteArray(privKey.getModulus))
    // CKA_PRIVATE_EXPONENT
    p11Key.getPrivateExponent.setByteArrayValue(iaik.pkcs.pkcs11.Util.unsignedBigIntergerToByteArray(privKey.getPrivateExponent))
    // CKA_PUBLIC_EXPONENT --> Assuming 65537
    //p11Key.getPublicExponent.setByteArrayValue(Array[Byte](0x01, 0x00, 0x01))
    p11Key.getPublicExponent.setByteArrayValue(iaik.pkcs.pkcs11.Util.unsignedBigIntergerToByteArray(pubKey.getPublicExponent))
    // CKA_PRIME_1
    p11Key.getPrime1.setByteArrayValue(iaik.pkcs.pkcs11.Util.unsignedBigIntergerToByteArray(crtPrivateKey.getPrimeExponentP))
    // CKA_PRIME_2
    p11Key.getPrime2.setByteArrayValue(iaik.pkcs.pkcs11.Util.unsignedBigIntergerToByteArray(crtPrivateKey.getPrimeQ))
    // CKA_EXPONENT_1
    p11Key.getExponent1.setByteArrayValue(iaik.pkcs.pkcs11.Util.unsignedBigIntergerToByteArray(crtPrivateKey.getPrimeExponentP))
    // CKA_EXPONENT_2
    p11Key.getExponent2.setByteArrayValue(iaik.pkcs.pkcs11.Util.unsignedBigIntergerToByteArray(crtPrivateKey.getPrimeExponentQ))
    // CKA_COEFFICIENT
    p11Key.getCoefficient.setByteArrayValue(iaik.pkcs.pkcs11.Util.unsignedBigIntergerToByteArray(crtPrivateKey.getCrtCoefficient))

    // Creating the Private key
    if (debug)
      println(s"[INFOS] Creating private key:\n${p11Key.toString}")
    session.createObject(p11Key)
  }

  private def actionImportKey(session: Session, debug: Boolean, p12Path: String, p12Password: String) = {
    val privKey = getPKCS12PrivateKey(p12Path, p12Password, debug)
    val pubKey = getPKCS12PublicKey(p12Path, p12Password, debug)
    privKey match {
      case Some(privk) => {
        pubKey match {
          case Some(pubk) => {
            importKey(session, privk, pubk, debug)
            println("[INFOS] Key successfuly imported")
          }
          case None => println("[ERROR] Key not imported!")
        }
      }
      case None => println("[ERROR] Key not imported!")
    }
  }

}


