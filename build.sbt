name := """p11tool"""

version := "1.1"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt" % "3.4.0",
  "com.typesafe.play" %% "play-json" % "2.5.3"
)

lazy val root = (project in file(".")).enablePlugins(JavaAppPackaging)

doc in Compile <<= target.map(_ / "none")

publishArtifact in(Compile, packageSrc) := false

mainClass in Compile := Some("fr.itassets.p11tool.P11Tool")
